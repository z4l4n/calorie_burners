package com.epam.calorie_burners.service;

import com.epam.calorie_burners.controller.TeamRegistrationControllerTest;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.repository.TeamRepository;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import com.epam.calorie_burners.service.implementation.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TeamServiceTest {

    @MockBean
    private TeamRepository repository;

    @Autowired
    private TeamServiceImpl service;

    private Team registration;


    @Test
    public void test_teamServiceShouldSendTeamRegistrationWithUniqueTeamName() throws TeamAlreadyRegisteredException {
        given_aValidTeamRegistration();
        when_registrationIsSentToTheService();
        then_registrationIsSentToTheRepo();
    }

    @Test(expected = TeamAlreadyRegisteredException.class)
    public void test_teamServiceShouldThrowExceptionWhenTeamNameIsAlreadyRegistered() throws TeamAlreadyRegisteredException {
        given_anAlreadyRegisteredTeam();
        when_registrationIsSentToTheService();
        then_registrationIsNotSentToTheRepo();
    }


    private void given_anAlreadyRegisteredTeam() {
        registration = TeamRegistrationControllerTest.composeValidTeamRegistration();
        when(repository.save(registration)).thenThrow(new DataIntegrityViolationException("test"));
    }

    private void given_aValidTeamRegistration() {
        registration = TeamRegistrationControllerTest.composeValidTeamRegistration();
    }

    private void when_registrationIsSentToTheService() throws TeamAlreadyRegisteredException {
        service.registerTeam(registration);
    }

    private void then_registrationIsNotSentToTheRepo() {
        verify(repository, times(0)).save(any());
    }

    private void then_registrationIsSentToTheRepo() {
        verify(repository, times(1)).save(registration);
    }


}
