package com.epam.calorie_burners.service;

import com.epam.calorie_burners.DTO.ActivityRegistrationDTO;
import com.epam.calorie_burners.DTO.DTOMapper;
import com.epam.calorie_burners.model.Activity;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.repository.ActivityRepository;
import com.epam.calorie_burners.service.implementation.ActivityServiceImpl;
import com.epam.calorie_burners.service.implementation.MemberServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ActivityServiceTest {

    private static final String DUMMY_EMAIL_ADDR = "test@test.com";
    private static final long DUMMY_TOTAL_CALORIES_BURNT = 99;
    private static final int DUMMY_TOTAL_TIME_SPENT_IN_MINUTES = 11;

    private long totalCaloriesBurntCurrentResult;
    private double totalTimeSpentCurrentResult;
    private boolean areThereRegisteredActivitesCurrentResult;
    private ActivityRegistrationDTO activityRegistrationDTO;
    private Member member;
    private Activity activity;

    @Mock
    private ActivityRepository mockActivityRepository;

    @Mock
    private MemberServiceImpl mockMemberService;

    @Mock
    private DTOMapper mockDtoMapper;

    @Mock
    private OidcUser oidcUser;

    @InjectMocks
    private ActivityServiceImpl activityService;


    @Test
    public void testRegisterActivityShouldCallMethods() {
        givenAnActivityRegistration();
        whenRegisterActivityIsCalled();
        thenAllMethodsAreCalled();
    }

    private void givenAnActivityRegistration() {
        activityRegistrationDTO = new ActivityRegistrationDTO();
        when(oidcUser.getEmail()).thenReturn(DUMMY_EMAIL_ADDR);

        member = new Member();
        when(mockMemberService.getMemberByEmail(DUMMY_EMAIL_ADDR)).thenReturn(member);

        activity = new Activity();
        when(mockDtoMapper.mapActivityRegistrationDTOtoActivity(activityRegistrationDTO, member)).thenReturn(activity);

    }

    private void whenRegisterActivityIsCalled() {
        activityService.registerActivity(activityRegistrationDTO, oidcUser);
    }

    private void thenAllMethodsAreCalled() {
        verify(mockMemberService, times(1)).getMemberByEmail(DUMMY_EMAIL_ADDR);

        verify(mockDtoMapper, times(1)).mapActivityRegistrationDTOtoActivity(activityRegistrationDTO, member);

        verify(mockActivityRepository, times(1)).save(activity);
    }

    @Test
    public void testGetTotalCaloriesBurnt() {
        givenRegisteredActivitiesForCalorieCalculation();
        whenGetTotalCaloriesBurntIsCalled();
        thenMethodCallIsDelegatedAndProperResultReturns();
    }

    private void givenRegisteredActivitiesForCalorieCalculation() {
        when(mockActivityRepository.getTotalCaloriesBurnt()).thenReturn(DUMMY_TOTAL_CALORIES_BURNT);
    }

    private void whenGetTotalCaloriesBurntIsCalled() {
        totalCaloriesBurntCurrentResult = activityService.getTotalCaloriesBurnt();
    }

    private void thenMethodCallIsDelegatedAndProperResultReturns() {
        verify(mockActivityRepository, times(1)).getTotalCaloriesBurnt();
        assertEquals(totalCaloriesBurntCurrentResult, DUMMY_TOTAL_CALORIES_BURNT);
    }

    @Test
    public void testGetTotalTimeSpentInHours() {
        givenRegisteredActivitiesForTotalSpentTimeCalculation();
        whenGetTotalTimeSpentIsCalled();
        thenMethodCallIsDelegatedAndMinutesAreConvertedToHours();
    }

    private void givenRegisteredActivitiesForTotalSpentTimeCalculation() {
        when(mockActivityRepository.getTotalTimeSpentInMinutes()).thenReturn(DUMMY_TOTAL_TIME_SPENT_IN_MINUTES);
    }

    private void whenGetTotalTimeSpentIsCalled() {
        totalTimeSpentCurrentResult = activityService.getTotalTimeSpentInHours();
    }

    private void thenMethodCallIsDelegatedAndMinutesAreConvertedToHours() {
        verify(mockActivityRepository, times(1)).getTotalTimeSpentInMinutes();
        assertEquals(totalTimeSpentCurrentResult, DUMMY_TOTAL_TIME_SPENT_IN_MINUTES / 60.0, 0.001);
    }

    @Test
    public void testIsThereRegisteredActivitiesShouldReturnTrueIfThereAre() {
        givenRegisteredActivities();
        whenIsThereRegisteredActivitiesMethodIsCalled();
        thenMethodIsCalledAndTrueIsReturned();
    }

    private void givenRegisteredActivities() {
        when(mockActivityRepository.isThereRegisteredActivity()).thenReturn(Integer.valueOf(1));
    }

    private void whenIsThereRegisteredActivitiesMethodIsCalled() {
        areThereRegisteredActivitesCurrentResult = activityService.isThereRegisteredActivity();
    }

    private void thenMethodIsCalledAndTrueIsReturned() {
        verify(mockActivityRepository, times(1)).isThereRegisteredActivity();
        assertTrue(areThereRegisteredActivitesCurrentResult);
    }

    @Test
    public void testIsThereRegisteredActivitiesShouldReturnFalseIfThereAreNot() {
        givenThereAreNoRegisteredActivites();
        whenIsThereRegisteredActivitiesMethodIsCalled();
        thenMethodIsCalledAndFalseIsReturned();
    }

    private void givenThereAreNoRegisteredActivites() {
        when(mockActivityRepository.isThereRegisteredActivity()).thenReturn(null);
    }

    private void thenMethodIsCalledAndFalseIsReturned() {
        verify(mockActivityRepository, times(1)).isThereRegisteredActivity();
        assertFalse(areThereRegisteredActivitesCurrentResult);
    }

}
