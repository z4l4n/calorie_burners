package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.ActivityType;
import com.epam.calorie_burners.repository.ActivityTypeRepository;
import com.epam.calorie_burners.service.implementation.ActivityTypeServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ActivityTypeServiceTest {

    @Mock
    private ActivityTypeRepository mockActivityTypeRepository;

    @InjectMocks
    private ActivityTypeServiceImpl activityTypeService;

    private static final String[] activityTypeNames = {"name1", "name2", "name3"};

    private List<ActivityType> registeredActivityTypes;

    private List<String> currentResult;


    @Test
    public void testGetRegisteredActivityTypeNamesShouldWork() {
        givenRegisteredActivityTypes();
        whenGetRegisteredActivityTypeNamesIsCalled();
        thenMethodIsCalledAndResultContainsAllTheNames();
    }

    private void givenRegisteredActivityTypes() {
        registeredActivityTypes = new ArrayList<>();

        for (String name : activityTypeNames) {
            ActivityType activityType = new ActivityType();
            activityType.setName(name);
            registeredActivityTypes.add(activityType);
        }

        when(mockActivityTypeRepository.findAll()).thenReturn(registeredActivityTypes);
    }

    private void whenGetRegisteredActivityTypeNamesIsCalled() {
        currentResult = activityTypeService.getRegisteredActivityTypeNames();
    }

    private void thenMethodIsCalledAndResultContainsAllTheNames() {
        verify(mockActivityTypeRepository, times(1)).findAll();
        assertTrue(currentResult.containsAll(Arrays.asList(activityTypeNames)));
    }


}
