package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.controller.TeamRegistrationControllerTest;
import com.epam.calorie_burners.model.Team;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RepositoryTest {

    @Autowired
    private TeamRepository repository;

    private Team registration;

    private DataIntegrityViolationException exception;

    @After
    public void cleanUp() {
        registration = null;
        exception = null;
    }

    @Test
    public void test_repositoryShouldStoreTeamWithUniqueName() {
        given_aValidTeamRegistrationWithUniqueName();
        when_repoSavesTheRegistration();
        then_registrationCanBeReadFromRepo();
    }

    @Test
    public void test_repositoryShouldThrowExceptionIfTeamNameIsAlreadyRegistered() {
        given_anAlreadyRegisteredTeam();
        when_alreadyRegisteredTeamIsSentToSaveAgain();
        then_exceptionIsThrown();
        then_registrationCanBeReadFromRepo(); // for deleting from repo
    }


    private void given_aValidTeamRegistrationWithUniqueName() {
        registration = TeamRegistrationControllerTest.composeValidTeamRegistration();
    }

    private void given_anAlreadyRegisteredTeam() {
        given_aValidTeamRegistrationWithUniqueName();
        when_repoSavesTheRegistration();
    }

    private void when_repoSavesTheRegistration() {
        try {
            repository.save(registration);
        } catch (DataIntegrityViolationException e) {
            exception = e;
        }
    }

    private void when_alreadyRegisteredTeamIsSentToSaveAgain() {
        createANewRegistrationWithSameName();
        when_repoSavesTheRegistration();
    }

    private void then_exceptionIsThrown() {
        assertTrue(exception != null);
    }


    private void then_registrationCanBeReadFromRepo() {
        Optional<Team> queryResult = repository.findByName(registration.getName());
        boolean isRegistrationStored = queryResult.isPresent();
        if (isRegistrationStored) {
            deleteRegistration(queryResult.get());
        }
        assertTrue(isRegistrationStored);
    }

    private void deleteRegistration(Team registration) {
        repository.delete(registration);
    }

    private void createANewRegistrationWithSameName() {
        String teamName = registration.getName();
        registration = new Team();
        registration.setName(teamName);
    }
}
