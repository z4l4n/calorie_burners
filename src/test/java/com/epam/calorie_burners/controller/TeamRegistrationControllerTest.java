package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.model.Person;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import com.epam.calorie_burners.service.implementation.TeamServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static com.epam.calorie_burners.controller.TeamRegistrationController.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamRegistrationController.class)

public class TeamRegistrationControllerTest {

    private static final String FORM_PARAM_TEAMNAME = "name";
    private static final String FORM_PARAM_MEMBER = "members[%s].name";
    private static final String INPUT_ERRORCLASS = "inputFieldErr";

    private ResultActions result;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private MessageSource messageSource;

    @MockBean
    private TeamServiceImpl service;

    private Team registration;


    @Test
    public void test_registrationWithValidDataShouldRedirectToConfirmationPage() throws Exception {
        given_aBlankTeamRegistrationForm();
        when_userSubmitsValidTeamRegistration();
        then_registrationIsSentToTheService();
        then_weAreRedirectedToConfirmationPage();
    }

    @Test
    public void test_registrationWithEmptyTeamNameShouldDisplayError() throws Exception {
        given_aBlankTeamRegistrationForm();
        when_userSubmitsTeamRegistrationWithEmptyTeamName();
        then_registrationPageIsDisplayed();
        then_teamNameErrorIsDisplayed();
        then_registrationIsNotSentToTheService();
    }

    @Test
    public void test_registrationWithEmptyMemberNameShouldDisplayError() throws Exception {
        given_aBlankTeamRegistrationForm();
        when_userSubmitsTeamRegistrationWithEmptyMemberName();
        then_memberNameErrorIsDisplayed();
        then_registrationPageIsDisplayed();
        then_registrationIsNotSentToTheService();
    }

    @Test
    public void test_registrationWithEmptyTeamNameAndMemberNameShouldDisplayError() throws Exception {
        given_aBlankTeamRegistrationForm();
        when_userSubmitsTeamRegistrationWithEmptyTeamAndMemberName();
        then_memberNameErrorIsDisplayed();
        then_teamNameErrorIsDisplayed();
        then_registrationPageIsDisplayed();
        then_registrationIsNotSentToTheService();
    }

    @Test
    public void test_registrationWithAlreadyRegisteredTeamNameShouldDisplayError() throws Exception {
        given_aBlankTeamRegistrationForm();
        when_userSubmitsTeamRegistrationWithAlreadyRegisteredTeamName();
        then_registrationPageIsDisplayed();
        then_registrationIsSentToTheService();
        then_teamNameAlreadyRegisteredErrorIsDisplayed();
    }


    @Test
    public void test_confirmationPageShouldContain5memberNamesWithURLs() throws Exception {
        test_registrationWithValidDataShouldRedirectToConfirmationPage();
        then_memberNamesAreDisplayed();

    }


    private void given_aBlankTeamRegistrationForm() throws Exception {
        result = mvc.perform(get(URL_TEAM_REGISTRATION).accept(MediaType.TEXT_HTML))
                    .andExpect(
                            status().isOk())
                    .andExpect(
                            view().name(VIEW_TEAM_REGISTRATION));
    }

    private void when_userSubmitsValidTeamRegistration() throws Exception {
        registration = composeValidTeamRegistration();
        submitForm(registration);
    }

    private void when_userSubmitsTeamRegistrationWithEmptyTeamName() throws Exception {
        registration = composeTeamRegistration("", 5);
        submitForm(registration);
    }

    private void when_userSubmitsTeamRegistrationWithEmptyMemberName() throws Exception {
        registration = composeTeamRegistration(UUID.randomUUID().toString(), 4);
        submitForm(registration);
    }

    private void when_userSubmitsTeamRegistrationWithEmptyTeamAndMemberName() throws Exception {
        registration = composeTeamRegistration("", 4);
        submitForm(registration);
    }

    private void when_userSubmitsTeamRegistrationWithAlreadyRegisteredTeamName() throws Exception {
        registration = composeTeamRegistration("team", 5);
        doThrow(new TeamAlreadyRegisteredException()).when(service).registerTeam(registration);
        submitForm(registration);
    }

    private void then_memberNamesAreDisplayed() throws Exception {
        for (Person member : registration.getMembers()) {
            result.andExpect(
                    xpath("//table/td[contains(text(), '" + member.getName() + "')]").exists());
        }
    }

    private void then_teamNameAlreadyRegisteredErrorIsDisplayed() throws Exception {
        String text_team_name_error = messageSource.getMessage("err.already_registered_name", null,
                                                               null);  // couldn't load messages from properties in test..
        result.andExpect(
                xpath("//form/input[1]/@class")
                        .string(INPUT_ERRORCLASS)).andExpect(
                xpath("//form/div[contains(text(), '" + text_team_name_error + "')]").exists());
    }

    private void then_weAreRedirectedToConfirmationPage() throws Exception {
        result.andExpect(redirectedUrl(TeamRegistrationController.URL_TEAM_REGISTRATION_CONFIRMATION));
    }


    private void then_registrationPageIsDisplayed() throws Exception {
        result.andExpect(view().name(TeamRegistrationController.VIEW_TEAM_REGISTRATION));
    }

    private void then_teamNameErrorIsDisplayed() throws Exception {
        String text_team_name_error = messageSource.getMessage("err.empty.team_name", null,
                                                               null);  // couldn't load messages from properties in test..
        result.andExpect(
                xpath("//form/input[1]/@class")
                        .string(INPUT_ERRORCLASS)).andExpect(
                xpath("//form/div[text()[contains(., '" + text_team_name_error + "')]]").exists());

    }

    private void then_memberNameErrorIsDisplayed() throws Exception {
        String text_member_name_error = messageSource.getMessage("err.empty.member_name", null,
                                                                 null);  // couldn't load messages from properties in test..
        result.andExpect(
                xpath("//form/input[6]/@class")
                        .string(INPUT_ERRORCLASS)).andExpect(
                xpath("//form/div[text()[contains(., '" + text_member_name_error + "')]]").exists());
    }

    private void then_registrationIsSentToTheService() throws TeamAlreadyRegisteredException {
        verify(service, times(1)).registerTeam(registration);
    }

    private void then_registrationIsNotSentToTheService() throws TeamAlreadyRegisteredException {
        verify(service, times(0)).registerTeam(any());
    }

    public static Team composeValidTeamRegistration() {
        return composeTeamRegistration(UUID.randomUUID().toString(), 5);
    }

    private static Team composeTeamRegistration(String teamName, int filledMemberCount) {
        Team result = new Team();
        result.setName(teamName);
        List<Person> members = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Person person = new Person();
            person.setName(i < filledMemberCount ? UUID.randomUUID().toString() : "");
            members.add(person);
        }
        result.setMembers(members);
        return result;
    }


    private MultiValueMap<String, String> assembleRequestParamsFromEntity(Team registration) {
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();
        result.add(FORM_PARAM_TEAMNAME, registration.getName());

        for (int i = 0; i < registration.getMembers().size(); i++) {
            Person member = registration.getMembers().get(i);
            result.add(String.format(FORM_PARAM_MEMBER, i), member.getName());
        }
        return result;
    }

    private void submitForm(Team teamDataToSubmit) throws Exception {
        result = mvc.perform(post(URL_TEAM_REGISTRATION_SUBMISSION).contentType(MediaType.APPLICATION_FORM_URLENCODED)
                                                                   .accept(MediaType.TEXT_HTML)
                                                                   .locale(Locale.forLanguageTag("hu"))
                                                                   .params(assembleRequestParamsFromEntity(
                                                                           teamDataToSubmit)));
    }
}
