package com.epam.calorie_burners.model;


import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(unique = true)
    @NotNull
    private String name;

    @OneToMany(mappedBy = "memberOf", cascade = CascadeType.PERSIST)
    private List<@Valid Person> members;


    public long getTotalCaloriesBurnt() {
        return members.stream().mapToLong(Person::getTotalCaloriesBurnt).sum();
    }

    public int getTotalMinutesSpent() {
        return members.stream().mapToInt(Person::getTotalMinutesSpent).sum();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getMembers() {
        return members;
    }

    public void setMembers(List<Person> members) {
        this.members = members;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                ", members=" + members +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Team team = (Team) o;

        if (id != team.id) {
            return false;
        }
        if (!name.equals(team.name)) {
            return false;
        }
        return members != null ? members.equals(team.members) : team.members == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + (members != null ? members.hashCode() : 0);
        return result;
    }

    public static void setMemberReferencesToTheirTeam(Team team) {
        team.getMembers().forEach(member -> member.setMemberOf(team));
    }


}
