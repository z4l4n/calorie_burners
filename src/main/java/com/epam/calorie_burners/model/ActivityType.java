package com.epam.calorie_burners.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class ActivityType {

    @Column(unique = true)
    @Id
    @NotBlank
    private String name;

    private int caloriesBurntPerMinutes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCaloriesBurntPerMinutes() {
        return caloriesBurntPerMinutes;
    }

    public void setCaloriesBurntPerMinutes(int caloriesBurntPerMinutes) {
        this.caloriesBurntPerMinutes = caloriesBurntPerMinutes;
    }

}
