package com.epam.calorie_burners.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @NotNull
    private String name;

    @OneToMany(mappedBy = "completedBy")
    private List<Activity> activities;

    @ManyToOne
    private Team memberOf;


    public int getTotalCaloriesBurnt() {
        return activities.stream().mapToInt(Activity::getCaloriesBurnt).sum();
    }

    public int getTotalMinutesSpent() {
        return activities.stream().mapToInt(Activity::getMinutesSpent).sum();
    }

    public Team getMemberOf() {
        return memberOf;
    }

    public void setMemberOf(Team memberOf) {
        this.memberOf = memberOf;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Person person = (Person) o;

        if (id != person.id) {
            return false;
        }
        return name.equals(person.name);
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return name;
    }
}
