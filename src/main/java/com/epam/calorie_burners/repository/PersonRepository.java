package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.model.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {

    @Query("SELECT p from Person p JOIN p.activities a GROUP BY p ORDER BY SUM(a.minutesSpent) DESC")
    Page<Person> getAllTPersonOrderedByTimeSpentInDescendingOrder(Pageable pageable);
    
}
