package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.model.Activity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ActivityRepository extends CrudRepository<Activity, Long> {

    @Query("SELECT COALESCE(SUM(t.caloriesBurntPerMinutes * a.minutesSpent), 0) FROM Activity a JOIN a.type t")
    long getTotalCaloriesBurnt();

    @Query("SELECT COALESCE(SUM(a.minutesSpent), 0) FROM Activity a")
    int getTotalTimeSpentInMinutes();

    @Query("SELECT 1 from Activity")
    Integer isThereRegisteredActivity();
}
