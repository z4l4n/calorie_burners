package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.model.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface MemberRepository extends CrudRepository<Member, Long> {

    @Query("SELECT p from Member p JOIN p.activities a GROUP BY p ORDER BY SUM(a.minutesSpent) DESC")
    Page<Member> getAllMembersOrderedByTimeSpentInDescendingOrder(Pageable pageable);

    @Query("SELECT COALESCE(SUM(a.minutesSpent * at.caloriesBurntPerMinutes), 0) FROM Member p JOIN p.activities a JOIN a.type at WHERE p.email = :email")
    long getTotalCaloriesBurnt(@Param("email") String email);

    @Query("SELECT COALESCE(SUM(a.minutesSpent), 0) FROM Member p JOIN p.activities a WHERE p.email = :email")
    long getTotalMinutesSpent(@Param("email") String email);

    Optional<Member> findByEmail(String email);

}