package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.model.ActivityType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ActivityTypeRepository extends CrudRepository<ActivityType, String> {
    List<ActivityType> findAll();

    Optional<ActivityType> findByName(String name);
}
