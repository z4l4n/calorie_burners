package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.model.ActivityType;
import org.springframework.data.repository.CrudRepository;

public interface ActivityTypeRepository extends CrudRepository<ActivityType, Long> {
}
