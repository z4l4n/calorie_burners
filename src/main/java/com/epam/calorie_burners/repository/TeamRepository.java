package com.epam.calorie_burners.repository;

import com.epam.calorie_burners.model.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface TeamRepository extends CrudRepository<Team, Long> {
    Optional<Team> findByName(String name);

    @Query("SELECT t FROM Team t JOIN t.members m JOIN m.activities a GROUP BY t ORDER BY SUM(a.minutesSpent) DESC")
    Page<Team> getAllTeamsOrderedByTimeSpentInDescendingOrder(Pageable pageable);

    @Query("SELECT COALESCE(SUM(a.minutesSpent * at.caloriesBurntPerMinutes), 0) FROM Team t JOIN t.members p JOIN p.activities a JOIN a.type at WHERE t.id = :teamId")
    long getTotalCaloriesBurnt(@Param("teamId") long teamId);

    @Query("SELECT COALESCE(SUM(a.minutesSpent), 0) FROM Team t JOIN t.members p JOIN p.activities a WHERE t.id = :teamId")
    long getTotalMinutesSpent(@Param("teamId") long teamId);


}
