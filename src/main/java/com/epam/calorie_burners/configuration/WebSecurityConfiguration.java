package com.epam.calorie_burners.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    // @Qualifier("authorityPopulatingUserService")
    private OidcUserService oidcUserService;


    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/css/**", "/", "/error", "/fonts/**", "/images/**").permitAll()
            .anyRequest().authenticated().and().csrf().disable().logout().logoutSuccessUrl("/").permitAll().and()
            .oauth2Login().loginPage("/sign-in").permitAll().userInfoEndpoint().oidcUserService(oidcUserService);
    }
}
