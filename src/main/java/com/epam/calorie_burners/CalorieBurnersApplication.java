package com.epam.calorie_burners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalorieBurnersApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalorieBurnersApplication.class, args);
	}
}
