package com.epam.calorie_burners.DTO;

public class MemberScoreboardItemDTO {

    private String name;

    private int minutes;

    private long calories;


    public MemberScoreboardItemDTO(String name, int minutes, long calories) {
        this.name = name;
        this.minutes = minutes;
        this.calories = calories;
    }

    public MemberScoreboardItemDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public long getCalories() {
        return calories;
    }

    public void setCalories(long calories) {
        this.calories = calories;
    }
}
