package com.epam.calorie_burners.DTO;

public class TotalStatisticsDTO {
    private long caloriesBurnt;

    private double hoursSpent;

    public long getCaloriesBurnt() {
        return caloriesBurnt;
    }

    public void setCaloriesBurnt(long caloriesBurnt) {
        this.caloriesBurnt = caloriesBurnt;
    }

    public double getHoursSpent() {
        return hoursSpent;
    }

    public void setHoursSpent(double hoursSpent) {
        this.hoursSpent = hoursSpent;
    }
}
