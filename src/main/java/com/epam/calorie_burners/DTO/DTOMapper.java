package com.epam.calorie_burners.DTO;

import com.epam.calorie_burners.model.Activity;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.ActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class DTOMapper {
    @Autowired
    private ActivityTypeService activityTypeService;

    public List<TeamScoreboardItemDTO> mapToTeamScoreboardDTO(List<Team> teams, List<Long> caloriesBurnt, List<Integer> minutesSpent) {
        List<TeamScoreboardItemDTO> result = new ArrayList();

        IntStream.of(teams.size() - 1).forEach(i -> {
            TeamScoreboardItemDTO scoreboardItem = new TeamScoreboardItemDTO();
            scoreboardItem.setId(teams.get(i).getId());
            scoreboardItem.setName(teams.get(i).getName());
            scoreboardItem.setMinutes(minutesSpent.get(i));
            scoreboardItem.setCalories(caloriesBurnt.get(i));
            result.add(scoreboardItem);
        });

        return result;
    }

    public List<MemberScoreboardItemDTO> mapToMemberScoreboardDTO(List<Member> members, List<Long> caloriesBurnt, List<Integer> minutesSpent) {
        List<MemberScoreboardItemDTO> result = new ArrayList();

        IntStream.of(members.size() - 1).forEach(i -> {
            MemberScoreboardItemDTO scoreboardItem = new TeamScoreboardItemDTO();
            scoreboardItem.setName(members.get(i).getName());
            scoreboardItem.setMinutes(minutesSpent.get(i));
            scoreboardItem.setCalories(caloriesBurnt.get(i));
            result.add(scoreboardItem);
        });
        return result;
    }

    public Activity mapActivityRegistrationDTOtoActivity(ActivityRegistrationDTO activityRegistrationDTO, Member activityOwner) {
        Activity result = new Activity();
        result.setCompletedBy(activityOwner);
        result.setDate(activityRegistrationDTO.getDate());
        result.setMinutesSpent(activityRegistrationDTO.getMinutesSpent());
        result.setType(activityTypeService.getActivityTypeByName(activityRegistrationDTO.getActivityTypeName()));
        return result;
    }


}
