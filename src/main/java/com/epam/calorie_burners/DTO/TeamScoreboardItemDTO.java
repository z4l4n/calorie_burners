package com.epam.calorie_burners.DTO;

public class TeamScoreboardItemDTO extends MemberScoreboardItemDTO {

    private long id;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
