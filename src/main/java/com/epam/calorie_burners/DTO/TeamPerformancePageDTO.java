package com.epam.calorie_burners.DTO;

import java.util.List;

public class TeamPerformancePageDTO {
    private String teamName;
    private List<MemberScoreboardItemDTO> memberScoreboard;
    private long totalCaloriesBurnt;
    private double totalHoursSpent;

    public TeamPerformancePageDTO(String teamName, List<MemberScoreboardItemDTO> memberScoreboard, long totalCaloriesBurnt, double totalHoursSpent) {
        this.teamName = teamName;
        this.memberScoreboard = memberScoreboard;
        this.totalCaloriesBurnt = totalCaloriesBurnt;
        this.totalHoursSpent = totalHoursSpent;
    }

    public long getTotalCaloriesBurnt() {
        return totalCaloriesBurnt;
    }

    public void setTotalCaloriesBurnt(long totalCaloriesBurnt) {
        this.totalCaloriesBurnt = totalCaloriesBurnt;
    }

    public double getTotalHoursSpent() {
        return totalHoursSpent;
    }

    public void setTotalHoursSpent(double totalHoursSpent) {
        this.totalHoursSpent = totalHoursSpent;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public List<MemberScoreboardItemDTO> getMemberScoreboard() {
        return memberScoreboard;
    }

    public void setMemberScoreboard(List<MemberScoreboardItemDTO> memberScoreboard) {
        this.memberScoreboard = memberScoreboard;
    }
}
