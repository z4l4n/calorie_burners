package com.epam.calorie_burners.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorHandlerController implements ErrorController {
    @RequestMapping("/error")
    public String asd(HttpServletRequest request, Model model) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");

        String errorMessage = null;
        if (statusCode == 404) {
            errorMessage = "Content not found.";
        }

        if (statusCode == 500) {
            errorMessage = "Internal error.";
        }
        model.addAttribute("errorMessage", errorMessage);
        model.addAttribute("status", statusCode);
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "error";
    }
}
