package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.model.Person;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.PersonService;
import com.epam.calorie_burners.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    private static final String VIEW_HOME = "home";
    private static final String URL_HOME = "/";

    @Autowired
    private ActivityService activityService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private PersonService personService;


    @GetMapping(URL_HOME)
    String showWelcomePage(Model model) {
        if (activityService.isThereRegisteredActivity()) {
            getAndSetScoreboardData(model);
        }
        return VIEW_HOME;
    }



    private void getAndSetScoreboardData(Model model) {
        long totalCaloriesBurnt = activityService.getTotalCaloriesBurnt();
        double totalTimeSpentInHours = activityService.getTotalTimeSpentInHours();
        model.addAttribute("thereAreActivites", true);
        model.addAttribute("caloriesBurnt", totalCaloriesBurnt);
        model.addAttribute("timeSpent", totalTimeSpentInHours);

        List<Team> bestTeams = teamService.getNTeamsOrderedByTimeSpentInDescendingOrder(5);
        List<Person> bestMembers = personService.getNTeamMembersOrderedByTimeSpentInDescendingOrder(5);
        model.addAttribute("bestTeams", bestTeams);
        model.addAttribute("bestMembers", bestMembers);
    }
}
