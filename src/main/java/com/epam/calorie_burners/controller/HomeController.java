package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.DTO.MemberScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TeamScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TotalStatisticsDTO;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.HomeService;
import com.epam.calorie_burners.service.MemberService;
import com.epam.calorie_burners.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    private static final String VIEW_HOME = "home";
    private static final String URL_HOME = "/";

    @Autowired
    private ActivityService activityService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private MemberService personService;

    @Autowired
    private HomeService homeService;


    @GetMapping(URL_HOME)
    String showWelcomePage(Model model) {
        if (activityService.isThereRegisteredActivity()) {
            getAndSetScoreboardData(model);
        }
        return VIEW_HOME;
    }


    private void getAndSetScoreboardData(Model model) {
        TotalStatisticsDTO totalStatistics = homeService.getTotalStatistics();
        model.addAttribute("thereAreActivites", true);
        model.addAttribute("totalStatistics", totalStatistics);

        List<TeamScoreboardItemDTO> bestTeams = homeService.getTeamScoreBoard();
        List<MemberScoreboardItemDTO> bestMembers = homeService.getMemberScoreBoard();
        model.addAttribute("bestTeams", bestTeams);
        model.addAttribute("bestMembers", bestMembers);
    }
}
