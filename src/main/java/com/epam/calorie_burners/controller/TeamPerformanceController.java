package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.controller.exception.NotMemberOfATeamException;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.MemberService;
import com.epam.calorie_burners.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;

@Controller
public class TeamPerformanceController {
    private static final String URL_MY_TEAM_PERFORMANCE = "/team";
    private static final String URL_TEAM_PERFORMANCE = "/team/{teamId}";
    private static final String VIEW_TEAM_PERFORMANCE = "team_performance";
    private static final String VIEW_NOT_MEMBER = "you_are_not_member";
    private static final String REDIRECT_TEAM_PERFORMANCE_BY_ID = "redirect:/team/%s";

    @Autowired
    private TeamService teamService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private MemberService memberService;

    @GetMapping(URL_MY_TEAM_PERFORMANCE)
    public String showMyTeamPerformancePage(@AuthenticationPrincipal OidcUser userAccount) throws NotFoundException {
        long teamId;
        try {
            teamId = teamService.tryToGetTeamIdByEmail(userAccount.getEmail());
        } catch (NotMemberOfATeamException e) {
            return VIEW_NOT_MEMBER;
        }
        return String.format(REDIRECT_TEAM_PERFORMANCE_BY_ID, teamId);
    }


    @GetMapping(URL_TEAM_PERFORMANCE)
    public String showTeamPerformancePage(@PathVariable String teamId, Model model, HttpServletResponse response) throws NotFoundException {

        if (activityService.isThereRegisteredActivity()) {
            model.addAttribute("thereAreActivites", true);
        }
        model.addAttribute("teamPerformancePageData", teamService.getTeamPerformancePageData(teamId));

        return VIEW_TEAM_PERFORMANCE;
    }


}
