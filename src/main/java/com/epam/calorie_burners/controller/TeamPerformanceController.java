package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;

@Controller
public class TeamPerformanceController {
    private static final String URL_TEAM_PERFORMANCE = "/team/{teamId}";
    private static final String VIEW_TEAM_PERFORMANCE = "team_performance";

    @Autowired
    private TeamService teamService;

    @Autowired
    private ActivityService activityService;

    @GetMapping(URL_TEAM_PERFORMANCE)
    public String showTeamPerformancePage(@PathVariable String teamId, Model model, HttpServletResponse response) {
        Team requestedTeam = checkIfRequestedTeamExists(teamId);
        getAndSetDataToDisplay(model, requestedTeam);

        return VIEW_TEAM_PERFORMANCE;
    }

    private Team checkIfRequestedTeamExists(String teamId) throws NotFoundException {
        Team requestedTeam;
        try {
            requestedTeam = teamService.findTeamById(Long.valueOf(teamId));
        } catch (NumberFormatException ex) {
            throw new NotFoundException();
        }
        return requestedTeam;
    }

    private void getAndSetDataToDisplay(Model model, Team requestedTeam) {
        long totalCaloriesBurnt = teamService.getTotalCaloriesBurnt(requestedTeam.getId());
        double totalTimeSpentInHours = teamService.getTotalHoursSpent(requestedTeam.getId());

        model.addAttribute("teamName", requestedTeam.getName());
        model.addAttribute("timeSpent", totalTimeSpentInHours);
        model.addAttribute("caloriesBurnt", totalCaloriesBurnt);
        model.addAttribute("teamMembers", requestedTeam.getMembers());
        if (activityService.isThereRegisteredActivity()) {
            model.addAttribute("thereAreActivites", true);
        }
    }

}
