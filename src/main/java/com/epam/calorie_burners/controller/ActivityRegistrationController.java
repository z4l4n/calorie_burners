package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.model.Activity;
import com.epam.calorie_burners.model.Person;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.ActivityTypeService;
import com.epam.calorie_burners.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Controller
@SessionAttributes({"requestedPerson", "activityTypes"})
public class ActivityRegistrationController {

    public static final String URL_ACTIVITY_REGISTRATION = "/member/{personId}";
    public static final String URL_TEAM_REGISTRATION_SUBMISSION = "/submit-team-registration";
    public static final String URL_ACTIVITY_REGISTRATION_CONFIRMATION = "/activity-registration-confirmation";
    public static final String VIEW_ACTIVITY_REGISTRATION = "activity_registration";
    public static final String VIEW_REDIRECT_ACTIVITY_REGISTRATION_CONFIRMATION = "redirect:" + URL_ACTIVITY_REGISTRATION_CONFIRMATION;
    public static final String VIEW_ACTIVITY_REGISTRATION_CONFIRMATION = "activity_registration_confirmation";
    private final String ATTR_TEAM_BACKING_BEAN = "teamFormBean";

    @Autowired
    private PersonService personService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityTypeService activityTypeService;

    @GetMapping(URL_ACTIVITY_REGISTRATION)
    //@PreAuthorize("hasAuthority('ROLE_MANAGER')")
    public String showRegistrationPage(@PathVariable String personId, Model model, @ModelAttribute("activity") Activity activity) {

        Person requestedPerson = checkIfRequestedPersonExists(personId);
        getAndSetDataToDisplay(model, requestedPerson);
        return VIEW_ACTIVITY_REGISTRATION;
    }

    @PostMapping(URL_ACTIVITY_REGISTRATION)
    public String processRegistration(@ModelAttribute("activity") @Valid Activity activity, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return VIEW_ACTIVITY_REGISTRATION;
        }
        storeSuccessfullyValidatedActivity(activity, model);
        return VIEW_REDIRECT_ACTIVITY_REGISTRATION_CONFIRMATION;
    }

    @GetMapping(URL_ACTIVITY_REGISTRATION_CONFIRMATION)
    public String showConfirmationPage() {
        return VIEW_ACTIVITY_REGISTRATION_CONFIRMATION;
    }


    private Person checkIfRequestedPersonExists(String personId) throws NotFoundException {
        Person requestedPerson;
        try {
            requestedPerson = personService.findPersonById(Long.valueOf(personId));
        } catch (NumberFormatException ex) {
            throw new NotFoundException();
        }
        return requestedPerson;
    }

    private void storeSuccessfullyValidatedActivity(Activity activity, Model model) {
        Person activityOwner = (Person) model.asMap().get("requestedPerson");
        activity.setCompletedBy(activityOwner);
        activityService.registerActivity(activity);
    }

    private void getAndSetDataToDisplay(Model model, Person requestedPerson) {
        model.addAttribute("requestedPerson", requestedPerson);
        model.addAttribute("activityTypes", activityTypeService.getActivityTypes());
    }
}
