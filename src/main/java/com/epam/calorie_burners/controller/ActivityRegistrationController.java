package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.DTO.ActivityRegistrationDTO;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.ActivityTypeService;
import com.epam.calorie_burners.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.validation.Valid;


@Controller
@SessionAttributes({"requestedPerson", "activityTypes"})
public class ActivityRegistrationController {

    public static final String URL_ACTIVITY_REGISTRATION = "/member";
    public static final String URL_TEAM_REGISTRATION_SUBMISSION = "/submit-team-registration";
    public static final String URL_ACTIVITY_REGISTRATION_CONFIRMATION = "/activity-registration-confirmation";
    public static final String VIEW_ACTIVITY_REGISTRATION = "activity_registration";
    public static final String VIEW_REDIRECT_ACTIVITY_REGISTRATION_CONFIRMATION = "redirect:" + URL_ACTIVITY_REGISTRATION_CONFIRMATION;
    public static final String VIEW_ACTIVITY_REGISTRATION_CONFIRMATION = "activity_registration_confirmation";
    private final String ATTR_TEAM_BACKING_BEAN = "teamFormBean";
    private static final String VIEW_NOT_MEMBER = "you_are_not_member";

    @Autowired
    private MemberService personService;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityTypeService activityTypeService;

    @GetMapping(URL_ACTIVITY_REGISTRATION)
    public String showRegistrationPage(Model model, @ModelAttribute("activityRegistrationDTO") ActivityRegistrationDTO activityRegistrationDTO, @AuthenticationPrincipal OidcUser completersAccount) {
        if (!personService.isRegisteredEmail(completersAccount.getEmail())) {
            return VIEW_NOT_MEMBER;
        }
        model.addAttribute("memberName", completersAccount.getFullName());
        model.addAttribute("activityTypeNames", activityTypeService.getRegisteredActivityTypeNames());

        return VIEW_ACTIVITY_REGISTRATION;
    }

    @PostMapping(URL_ACTIVITY_REGISTRATION)
    public String processRegistration(@ModelAttribute("activityRegistrationDTO") @Valid ActivityRegistrationDTO activityRegistrationDTO, BindingResult bindingResult, @AuthenticationPrincipal OidcUser completersAccount) {
        if (bindingResult.hasErrors()) {
            return VIEW_ACTIVITY_REGISTRATION;
        }

        activityService.registerActivity(activityRegistrationDTO, completersAccount);
        return VIEW_REDIRECT_ACTIVITY_REGISTRATION_CONFIRMATION;
    }

    @GetMapping(URL_ACTIVITY_REGISTRATION_CONFIRMATION)
    public String showConfirmationPage(@AuthenticationPrincipal OidcUser completersAccount, Model model) {
        model.addAttribute("memberName", completersAccount.getFullName());
        return VIEW_ACTIVITY_REGISTRATION_CONFIRMATION;
    }


}
