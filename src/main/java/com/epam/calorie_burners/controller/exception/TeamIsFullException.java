package com.epam.calorie_burners.controller.exception;

public class TeamIsFullException extends Exception {
    public TeamIsFullException() {
    }

    public TeamIsFullException(String message) {
        super(message);
    }

    public TeamIsFullException(String message, Throwable cause) {
        super(message, cause);
    }
}
