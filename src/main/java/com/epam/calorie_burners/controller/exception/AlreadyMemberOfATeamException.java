package com.epam.calorie_burners.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AlreadyMemberOfATeamException extends Exception {
    public AlreadyMemberOfATeamException() {
    }

    public AlreadyMemberOfATeamException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlreadyMemberOfATeamException(String message) {
        super(message);
    }
}
