package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.controller.exception.AlreadyMemberOfATeamException;
import com.epam.calorie_burners.controller.exception.EmptyTeamNameException;
import com.epam.calorie_burners.service.MemberService;
import com.epam.calorie_burners.service.TeamService;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes("storedTeamId")
public class TeamRegistrationController {

    public static final String URL_TEAM_REGISTRATION = "/team-registration";
    public static final String URL_TEAM_REGISTRATION_SUBMISSION = "/submit-team-registration";
    public static final String URL_TEAM_REGISTRATION_CONFIRMATION = "/team-registration-confirmation";
    public static final String VIEW_TEAM_REGISTRATION = "team_registration";
    public static final String VIEW_REDIRECT_TEAM_REGISTRATION_CONFIRMATION = "redirect:" + URL_TEAM_REGISTRATION_CONFIRMATION;
    public static final String VIEW_TEAM_REGISTRATION_CONFIRMATION = "team_registration_confirmation";

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private TeamService teamService;

    @Autowired
    private MemberService personService;

    @GetMapping(URL_TEAM_REGISTRATION)
    String showTeamRegistrationPage() {
        return VIEW_TEAM_REGISTRATION;
    }

    @PostMapping(URL_TEAM_REGISTRATION_SUBMISSION)
    public String processTeamRegistrationForm(Model model, @RequestParam String teamName, @AuthenticationPrincipal OidcUser teamLeadersAccount) {
        String nextView = tryToStoreTeamAndPrepareView(teamName, model, teamLeadersAccount);
        return nextView;
    }

    @GetMapping(URL_TEAM_REGISTRATION_CONFIRMATION)
    public String showTeamRegistrationConfirmation(Model model, HttpServletRequest request) throws MalformedURLException, URISyntaxException {
        setConfirmationDataToDisplay(request, model);
        return VIEW_TEAM_REGISTRATION_CONFIRMATION;
    }

    private void setConfirmationDataToDisplay(HttpServletRequest request, Model model) throws MalformedURLException, URISyntaxException {
        model.addAttribute("joiningURLSchema", teamService.composeJoiningLinkURLSchema(request));
    }

    private String tryToStoreTeamAndPrepareView(String teamName, Model model, OidcUser teamLeadersSocialAccount) {
        List<String> errorMessages = new ArrayList<>();
        long storedTeamId = 0;

        try {
            storedTeamId = teamService.tryToStoreTeam(teamName, teamLeadersSocialAccount);
        } catch (TeamAlreadyRegisteredException ex) {
            errorMessages.add(messageSource.getMessage("err.already_registered_team", null, null));
        } catch (AlreadyMemberOfATeamException ex) {
            errorMessages.add(messageSource.getMessage("err.already_member", null, null));
        } catch (EmptyTeamNameException ex) {
            errorMessages.add(messageSource.getMessage("err.empty_team_name", null, null));
        }

        if (!errorMessages.isEmpty()) {
            model.addAttribute("errorMessages", errorMessages);
            return VIEW_TEAM_REGISTRATION;
        }

        model.addAttribute("storedTeamId", storedTeamId);
        return VIEW_REDIRECT_TEAM_REGISTRATION_CONFIRMATION;
    }
}
