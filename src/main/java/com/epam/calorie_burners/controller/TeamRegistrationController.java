package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.TeamService;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Controller
@SessionAttributes("registeredTeam")

public class TeamRegistrationController {

    public static final String URL_TEAM_REGISTRATION = "/team-registration";
    public static final String URL_TEAM_REGISTRATION_SUBMISSION = "/submit-team-registration";
    public static final String URL_TEAM_REGISTRATION_CONFIRMATION = "/team-registration-confirmation";
    public static final String VIEW_TEAM_REGISTRATION = "team_registration";
    public static final String VIEW_REDIRECT_TEAM_REGISTRATION_CONFIRMATION = "redirect:" + URL_TEAM_REGISTRATION_CONFIRMATION;
    public static final String VIEW_TEAM_REGISTRATION_CONFIRMATION = "team_registration_confirmation";
    private static final String ATTR_TEAM_BACKING_BEAN = "teamFormBean";

    @Autowired
    private TeamService teamService;

    private ErrorMessageHandler errorMessageHandler = new ErrorMessageHandler();

    @GetMapping(URL_TEAM_REGISTRATION)
    String showTeamRegistrationPage(@ModelAttribute(ATTR_TEAM_BACKING_BEAN) Team formBean) {
        return VIEW_TEAM_REGISTRATION;
    }

    @PostMapping(URL_TEAM_REGISTRATION_SUBMISSION)
    public String processTeamRegistrationForm(Model model, @Valid @ModelAttribute(ATTR_TEAM_BACKING_BEAN) Team formBean, BindingResult result, HttpServletResponse response) {
        response.setStatus(BAD_REQUEST.value());
        if (result.hasErrors()) {
            errorMessageHandler.mapFieldErrorsToGlobalErrors(result);
            return VIEW_TEAM_REGISTRATION;
        }
        try {
            tryToStoreFieldValidatedTeamAndPrepareToDisplay(formBean, model, response);
            return VIEW_REDIRECT_TEAM_REGISTRATION_CONFIRMATION;
        } catch (TeamAlreadyRegisteredException e) {
            errorMessageHandler.setTeamAlreadyRegisteredError(result);
            return VIEW_TEAM_REGISTRATION;
        }
    }

    @GetMapping(URL_TEAM_REGISTRATION_CONFIRMATION)
    public String showTeamRegistrationConfirmation(Model model, HttpServletRequest request) throws MalformedURLException, URISyntaxException {
        setConfirmationDataToDisplay(request, model);
        return VIEW_TEAM_REGISTRATION_CONFIRMATION;
    }


    @InitBinder(ATTR_TEAM_BACKING_BEAN)
    private void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    private void setConfirmationDataToDisplay(HttpServletRequest request, Model model) throws MalformedURLException, URISyntaxException {
        model.addAttribute("personalURLSchema", teamService.composePersonalPageURLSchema(request));
        model.addAttribute("teamURLSchema", teamService.composeTeamPageURLSchema(request));
    }

    private void tryToStoreFieldValidatedTeamAndPrepareToDisplay(Team team, Model model, HttpServletResponse response) throws TeamAlreadyRegisteredException {
        Team.setMemberReferencesToTheirTeam(team);
        Team storedTeam = teamService.registerTeam(team);
        model.addAttribute("registeredTeam", storedTeam);
        response.setStatus(HttpStatus.OK.value());
    }


    private class ErrorMessageHandler {

        private void mapFieldErrorsToGlobalErrors(BindingResult result) {
            List<FieldError> fieldErrors = result.getFieldErrors();

            if (fieldErrors.stream().anyMatch(fieldError -> fieldError.getField().startsWith("member"))) {
                result.reject("err.empty.member_name",
                              "You must fill all the members");  // def message is here only because of a test-problem
            }

            if (fieldErrors.stream().anyMatch(fieldError -> fieldError.getField().startsWith("name"))) {
                result.reject("err.empty.team_name",
                              "Team name cannot be empty");  // def message is here only because of a test-problem
            }
        }

        private void setTeamAlreadyRegisteredError(BindingResult result) {
            result.rejectValue("name", "err.already_registered_name", "Team name is already registered");
            result.reject("err.already_registered_name", "Team name is already registered");
        }
    }
}
