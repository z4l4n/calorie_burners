package com.epam.calorie_burners.controller;

import com.epam.calorie_burners.controller.exception.AlreadyMemberOfATeamException;
import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.controller.exception.TeamIsFullException;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.MemberService;
import com.epam.calorie_burners.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes(names = {"updatedTeam"})
public class JoiningController {

    private static final String VIEW_JOINING = "joining_a_team";

    private static final String VIEW_JOINING_CONFIRMATION = "joining_confirmation";

    @Autowired
    private JoiningValidator joiningValidator;

    @Autowired
    private TeamService teamService;

    @Autowired
    private MemberService personService;

    @GetMapping("/join/{teamId}")
    public String showJoiningPage(Model model, @PathVariable String teamId, @AuthenticationPrincipal OidcUser currentLoggedInAccount) throws NotFoundException {
        Team requestedTeam = teamService.findTeamById(teamId);
        Member person = personService.createMemberOrGetIfExistsFromAccount(currentLoggedInAccount);
        checkJoiningConditionsAndPrepeareView(model, requestedTeam, person);

        return VIEW_JOINING;
    }

    @GetMapping("/join")
    public String showConfirmationPage(Model model, @AuthenticationPrincipal OidcUser currentLoggedInAccount) throws NotFoundException {
        if (model.asMap().get("updatedTeam") == null) {
            throw new NotFoundException();
        }
        Team updatedTeam = (Team) model.asMap().get("updatedTeam");
        teamService.updateTeam(updatedTeam);

        return VIEW_JOINING_CONFIRMATION;
    }


    private void checkJoiningConditionsAndPrepeareView(Model model, Team team, Member person) {
        model.addAttribute("team", team);
        try {
            joiningValidator.validateJoining(team, person);
        } catch (TeamIsFullException ex) {
            model.addAttribute("isTeamFull", true);
        } catch (AlreadyMemberOfATeamException ex) {
            model.addAttribute("isAlreadyMember", true);
            model.addAttribute("person", person);
        }
        teamService.addMemberToTeam(person, team);
        model.addAttribute("updatedTeam", team);
    }


    @Component
    private class JoiningValidator {
        public void validateJoining(Team team, Member person) throws TeamIsFullException, AlreadyMemberOfATeamException {
            if (personService.isRegisteredEmail(person.getEmail())) {
                throw new AlreadyMemberOfATeamException();
            }

            if (team.getMembers().size() == 5) {
                throw new TeamIsFullException();
            }
        }
    }


}
