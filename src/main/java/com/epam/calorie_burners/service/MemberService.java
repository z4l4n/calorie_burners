package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.Member;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import java.util.List;

public interface MemberService {

    Member getMemberByEmail(String email);

    Member createMemberOrGetIfExistsFromAccount(OidcUser personAccount);

    Member createMemberFromOidcUser(OidcUser oidcUser);

    boolean isRegisteredEmail(String email);

    List<Member> getTop5MembersByTimeSpent();

    List<Long> getCaloriesBurntByMembers(List<Member> members);

    List<Integer> getMinutesSpentWithActivityByMembers(List<Member> members);

    long getTotalCaloriesBurnt(String email);

    double getTotalHoursSpent(String email);
}
