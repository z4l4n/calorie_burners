package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.DTO.DTOMapper;
import com.epam.calorie_burners.DTO.MemberScoreboardItemDTO;
import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.repository.MemberRepository;
import com.epam.calorie_burners.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.data.domain.PageRequest.of;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private DTOMapper dtoMapper;

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public Member getMemberByEmail(String email) {
        Optional<Member> result = memberRepository.findByEmail(email);
        if (!result.isPresent()) {
            throw new NotFoundException(String.format("Member not found with email '%s'", email));
        }
        return result.get();
    }


    public List<MemberScoreboardItemDTO> getMemberScoreBoard() {
        List<Member> bestMembers = getTop5MembersByTimeSpent();
        List<Long> caloriesBurnt = getCaloriesBurntByMembers(bestMembers);

        List<Integer> minutesSpent = getMinutesSpentWithActivityByMembers(bestMembers);

        List<MemberScoreboardItemDTO> result = dtoMapper.mapToMemberScoreboardDTO(bestMembers, caloriesBurnt,
                                                                                  minutesSpent);
        return result;
    }

    @Override
    public long getTotalCaloriesBurnt(String email) {
        return memberRepository.getTotalCaloriesBurnt(email);
    }

    @Override
    public double getTotalHoursSpent(String email) {
        return memberRepository.getTotalMinutesSpent(email) / 60.0;
    }

    @Override
    public Member createMemberOrGetIfExistsFromAccount(OidcUser memberAccount) {
        Optional<Member> storedMember = memberRepository.findByEmail(memberAccount.getEmail());
        if (storedMember.isPresent()) {
            return storedMember.get();
        }
        return createMemberFromOidcUser(memberAccount);
    }

    @Override
    public Member createMemberFromOidcUser(OidcUser oidcUser) {
        Member result = new Member();
        result.setEmail(oidcUser.getEmail());
        result.setName(oidcUser.getFullName());
        return result;
    }

    @Override
    public boolean isRegisteredEmail(String email) {
        Optional<Member> storedMember = memberRepository.findByEmail(email);
        return storedMember.isPresent();
    }

    @Override
    public List<Member> getTop5MembersByTimeSpent() {
        Pageable paging = of(0, 5);
        List<Member> bestMembers = memberRepository.getAllMembersOrderedByTimeSpentInDescendingOrder(paging)
                                                   .getContent();
        return bestMembers;
    }

    public List<Long> getCaloriesBurntByMembers(List<Member> members) {
        return members.stream().map(member -> getTotalCaloriesBurnt(member.getEmail())).collect(
                Collectors.toList());
    }

    public List<Integer> getMinutesSpentWithActivityByMembers(List<Member> members) {
        return members.stream().map(
                member -> (int) Math.round(getTotalHoursSpent(member.getEmail()) * 60)).collect(Collectors.toList());
    }


}
