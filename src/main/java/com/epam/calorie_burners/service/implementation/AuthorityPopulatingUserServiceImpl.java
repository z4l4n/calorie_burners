package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.service.UsersGrantedAuthoritiesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
//@Qualifier("authorityPopulatingUserService")
public class AuthorityPopulatingUserServiceImpl extends OidcUserService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UsersGrantedAuthoritiesService authorityMappingService;

    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {

        OidcUser user = super.loadUser(userRequest);
        logger.debug("for user {} the underlying default OidcUservice returned: {}", userRequest, user);

        Set<GrantedAuthority> authorities = authorityMappingService.determineGrantedAuthoritiesFor(user);
        OidcUser newUser = new DefaultOidcUser(authorities, user.getIdToken(), user.getUserInfo());
        logger.debug("user with the right Granted Authorities: {}", newUser);

        return newUser;
    }
}
