package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.DTO.DTOMapper;
import com.epam.calorie_burners.DTO.MemberScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TeamScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TotalStatisticsDTO;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.HomeService;
import com.epam.calorie_burners.service.MemberService;
import com.epam.calorie_burners.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HomeServiceImpl implements HomeService {
    @Autowired
    private ActivityService activityService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private DTOMapper dtoMapper;


    @Override
    public TotalStatisticsDTO getTotalStatistics() {
        TotalStatisticsDTO totalStatistics = new TotalStatisticsDTO();
        long totalCaloriesBurnt = activityService.getTotalCaloriesBurnt();
        double totalHoursSpent = activityService.getTotalTimeSpentInHours();

        totalStatistics.setCaloriesBurnt(totalCaloriesBurnt);
        totalStatistics.setHoursSpent(totalHoursSpent);

        return totalStatistics;
    }

    @Override
    public List<TeamScoreboardItemDTO> getTeamScoreBoard() {
        List<Team> bestTeams = teamService.getTop5TeamsByTimeSpent();
        List<Long> caloriesBurnt = teamService.getCaloriesBurntByTeams(bestTeams);
        List<Integer> minutesSpent = teamService.getMinutesSpentWithActivityByTeams(bestTeams);

        List<TeamScoreboardItemDTO> result = dtoMapper.mapToTeamScoreboardDTO(bestTeams, caloriesBurnt,
                                                                              minutesSpent);
        return result;
    }

    @Override
    public List<MemberScoreboardItemDTO> getMemberScoreBoard() {

        List<Member> bestMembers = memberService.getTop5MembersByTimeSpent();
        List<Long> caloriesBurnt = memberService.getCaloriesBurntByMembers(bestMembers);

        List<Integer> minutesSpent = memberService.getMinutesSpentWithActivityByMembers(bestMembers);

        List<MemberScoreboardItemDTO> result = dtoMapper.mapToMemberScoreboardDTO(bestMembers, caloriesBurnt,
                                                                                  minutesSpent);
        return result;
    }
}
