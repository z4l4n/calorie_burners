package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.model.ActivityType;
import com.epam.calorie_burners.repository.ActivityTypeRepository;
import com.epam.calorie_burners.service.ActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ActivityTypeServiceImpl implements ActivityTypeService {
    @Autowired
    private ActivityTypeRepository activityTypeRepository;

    @Override
    public List<String> getRegisteredActivityTypeNames() {
        return activityTypeRepository.findAll().stream().map(activityType -> activityType.getName()).collect(
                Collectors.toList());
    }

    @Override
    public ActivityType getActivityTypeByName(String activityName) {
        Optional<ActivityType> result = activityTypeRepository.findByName(activityName);
        return result.get();
    }
}
