package com.epam.calorie_burners.service.implementation;


import com.epam.calorie_burners.service.UsersGrantedAuthoritiesService;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.*;

@Service
@ConfigurationProperties("security")
@Validated
public class UsersGrantedAuthoritiesServiceImpl implements UsersGrantedAuthoritiesService {

    private Map<String, Set<GrantedAuthority>> authorities = new HashMap<>();

    private Set<GrantedAuthority> defaultAuthorities;

    public @NotNull
    Set<GrantedAuthority> determineGrantedAuthoritiesFor(final @NotNull OidcUser user) {

        Set<GrantedAuthority> grantedAuthorities = authorities.get(user.getEmail());
        grantedAuthorities = (grantedAuthorities == null) ? new HashSet<>() : new HashSet<>(grantedAuthorities);
        grantedAuthorities.addAll(defaultAuthorities);
        return Collections.unmodifiableSet(grantedAuthorities);
    }


    public Map<String, Set<GrantedAuthority>> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Map<String, Set<GrantedAuthority>> authorities) {
        this.authorities = authorities;
    }

    public Set<GrantedAuthority> getDefaultAuthorities() {
        return defaultAuthorities;
    }

    public void setDefaultAuthorities(Set<GrantedAuthority> defaultAuthorities) {
        this.defaultAuthorities = defaultAuthorities;
    }
}
