package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.model.ActivityType;
import com.epam.calorie_burners.repository.ActivityTypeRepository;
import com.epam.calorie_burners.service.ActivityTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityTypeServiceImplementation implements ActivityTypeService {
    @Autowired
    ActivityTypeRepository activityTypeRepository;

    @Override
    public Iterable<ActivityType> getActivityTypes() {
        return activityTypeRepository.findAll();
    }
}
