package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.repository.TeamRepository;
import com.epam.calorie_burners.service.TeamService;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.domain.PageRequest.of;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    private Logger logger = LoggerFactory.getLogger(TeamServiceImpl.class);


    @Override
    public Team registerTeam(Team team) throws TeamAlreadyRegisteredException {
        try {
            Team result = teamRepository.save(team);
            result.getMembers().size();
            logger.info(team + " has been stored in the database.");
            return result;
        } catch (DataIntegrityViolationException e) {
            String cause = String.format("Couldn't store the entity. Team with name '%s' has already been registered.",
                                         team.getName());
            logger.error(cause);
            throw new TeamAlreadyRegisteredException(cause, e);
        }
    }

    private String composeURLSchema(HttpServletRequest request, String path) throws URISyntaxException, MalformedURLException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(request.getScheme());
        builder.setHost(request.getServerName());
        builder.setPort(request.getServerPort());
        builder.setPath(request.getContextPath() + String.format("/%s/", path));
        return builder.build().toURL().toString();
    }

    @Override
    public String composePersonalPageURLSchema(HttpServletRequest request) throws URISyntaxException, MalformedURLException {
        return composeURLSchema(request, "member");
    }

    @Override
    public String composeTeamPageURLSchema(HttpServletRequest request) throws URISyntaxException, MalformedURLException {
        return composeURLSchema(request, "team");
    }

    @Override
    public List<Team> getNTeamsOrderedByTimeSpentInDescendingOrder(int N) {
        Pageable paging = of(0, N);
        return teamRepository.getAllTeamsOrderedByTimeSpentInDescendingOrder(paging).getContent();
    }

    @Override
    @Transactional
    public Team findTeamById(long id) throws NotFoundException {
        Optional<Team> requestedTeam = teamRepository.findById(id);
        if (!requestedTeam.isPresent()) {
            String cause = String.format("There is no team with id: %s in the repository.", id);
            logger.info(cause);
            throw new NotFoundException(cause);
        }
        return requestedTeam.get();
    }

    @Override
    @Transactional
    public long getTotalCaloriesBurnt(long teamId) {
        return teamRepository.getTotalCaloriesBurnt(teamId);
    }

    @Override
    @Transactional
    public double getTotalHoursSpent(long teamId) {
        long minutesSpent = teamRepository.getTotalMinutesSpent(teamId);
        return minutesSpent / 60.0;
    }


}
