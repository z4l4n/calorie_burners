package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.DTO.DTOMapper;
import com.epam.calorie_burners.DTO.MemberScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TeamPerformancePageDTO;
import com.epam.calorie_burners.controller.exception.AlreadyMemberOfATeamException;
import com.epam.calorie_burners.controller.exception.EmptyTeamNameException;
import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.controller.exception.NotMemberOfATeamException;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.repository.MemberRepository;
import com.epam.calorie_burners.repository.TeamRepository;
import com.epam.calorie_burners.service.MemberService;
import com.epam.calorie_burners.service.TeamService;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.data.domain.PageRequest.of;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private DTOMapper dtoMapper;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private MemberService memberService;

    private Logger logger = LoggerFactory.getLogger(TeamServiceImpl.class);


    @Override
    /**
     * Tries to Compose a team from a teamName and an OidcUser account and store it
     * @return the id of the stored team
     *
     */
    public long tryToStoreTeam(String teamName, OidcUser teamLeadersOidc) throws TeamAlreadyRegisteredException, AlreadyMemberOfATeamException, EmptyTeamNameException {
        if (isEmptyTeamName(teamName)) {
            throw new EmptyTeamNameException();
        }

        if (isAlreadyMemberOfATeam(teamLeadersOidc)) {
            throw new AlreadyMemberOfATeamException();
        }

        Team team = composeTeam(teamName, teamLeadersOidc);
        long savedTeamId = tryToSaveTeamIntoRepository(team).getId();
        return savedTeamId;
    }


    @Override
    public String composeJoiningLinkURLSchema(HttpServletRequest request) throws URISyntaxException, MalformedURLException {
        return composeURLSchema(request, "join");
    }


    private String composeURLSchema(HttpServletRequest request, String path) throws URISyntaxException, MalformedURLException {
        URIBuilder builder = new URIBuilder();
        builder.setScheme(request.getScheme());
        builder.setHost(request.getServerName());
        builder.setPort(request.getServerPort());
        builder.setPath(request.getContextPath() + String.format("/%s/", path));
        return builder.build().toURL().toString();
    }

    @Override
    public List<Long> getCaloriesBurntByTeams(List<Team> teams) {
        return teams.stream().map(team -> getTotalCaloriesBurnt(team.getId())).collect(
                Collectors.toList());
    }

    @Override
    public List<Integer> getMinutesSpentWithActivityByTeams(List<Team> teams) {
        return teams.stream().map(
                team -> (int) Math.round(getTotalHoursSpent(team.getId()) * 60)).collect(Collectors.toList());
    }

    @Override
    public long tryToGetTeamIdByEmail(String email) throws NotMemberOfATeamException {
        Member member = null;
        try {
            member = memberService.getMemberByEmail(email);
        } catch (NotFoundException e) {
            throw new NotMemberOfATeamException();
        }
        return member.getMemberOf().getId();
    }

    @Override
    public TeamPerformancePageDTO getTeamPerformancePageData(String teamId) {
        Team team = findTeamById(teamId);

        final long convertedTeamId = Long.valueOf(teamId);
        long totalCaloriesBurnt = getTotalCaloriesBurnt(convertedTeamId);
        double totalHoursSpent = getTotalHoursSpent(convertedTeamId);

        List<MemberScoreboardItemDTO> memberScoreBoard = composeMemberScoreboard(team);

        return new TeamPerformancePageDTO(team.getName(), memberScoreBoard, totalCaloriesBurnt, totalHoursSpent);
    }

    private List<MemberScoreboardItemDTO> composeMemberScoreboard(Team team) {
        List<MemberScoreboardItemDTO> memberScoreboard = new ArrayList<>();
        team.getMembers().forEach(member -> {
            long caloriesBurnt = memberService.getTotalCaloriesBurnt(member.getEmail());
            int minutesSpent = (int) Math.round(memberService.getTotalHoursSpent(member.getEmail()) * 60);
            memberScoreboard.add(new MemberScoreboardItemDTO(member.getName(), minutesSpent, caloriesBurnt));
        });

        return memberScoreboard;
    }


    @Transactional
    @Override
    public Team findTeamById(String teamId) throws NotFoundException {
        Optional<Team> requestedTeam;
        try {
            requestedTeam = teamRepository.findById(Long.valueOf(teamId));
        } catch (NumberFormatException ex) {
            throw new NotFoundException();
        }
        if (!requestedTeam.isPresent()) {
            String cause = String.format("There is no team with id: %s in the repository.", teamId);
            logger.info(cause);
            throw new NotFoundException(cause);
        }
        return requestedTeam.get();
    }

    @Override
    @Transactional
    public long getTotalCaloriesBurnt(long teamId) {
        return teamRepository.getTotalCaloriesBurnt(teamId);
    }

    @Override
    @Transactional
    public double getTotalHoursSpent(long teamId) {
        long minutesSpent = teamRepository.getTotalMinutesSpent(teamId);
        return minutesSpent / 60.0;
    }

    @Override
    public void updateTeam(Team team) {
        teamRepository.save(team);
    }

    @Override
    public void addMemberToTeam(Member member, Team team) {
        team.getMembers().add(member);
        member.setMemberOf(team);
    }

    @Override
    public List<Team> getTop5TeamsByTimeSpent() {
        Pageable paging = of(0, 5);
        List<Team> bestTeams = teamRepository.getAllTeamsOrderedByTimeSpentInDescendingOrder(paging).getContent();
        return bestTeams;
    }

    private boolean isAlreadyMemberOfATeam(OidcUser teamLeadersAccount) {
        return memberService.isRegisteredEmail(teamLeadersAccount.getEmail());

    }

    private boolean isEmptyTeamName(String teamName) {
        return (teamName == null || teamName.isEmpty());
    }

    private Team composeTeam(String teamName, OidcUser teamLeadersAccount) {
        Member teamLeader = memberService.createMemberFromOidcUser(teamLeadersAccount);
        Team team = new Team();
        team.setName(teamName);
        team.setLeader(teamLeader);
        teamLeader.setMemberOf(team);
        return team;
    }

    private Team tryToSaveTeamIntoRepository(Team team) throws TeamAlreadyRegisteredException {
        try {
            return teamRepository.save(team);
        } catch (
                ConstraintViolationException ex) {
            throw new TeamAlreadyRegisteredException();
        } catch (Exception ex) {
            throw new TeamAlreadyRegisteredException();

        }
    }
}
