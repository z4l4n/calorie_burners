package com.epam.calorie_burners.service.implementation;

import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.model.Person;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.repository.PersonRepository;
import com.epam.calorie_burners.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.springframework.data.domain.PageRequest.of;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    PersonRepository personRepository;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    @Transactional
    public Person findPersonById(long id) throws NotFoundException {
        Optional<Person> result = personRepository.findById(id);
        if (!result.isPresent()) {
            String cause = String.format("There is no person with id: %s in the repository.", id);
            logger.info(cause);
            throw new NotFoundException(cause);
        }
        Team t = result.get().getMemberOf();

        return result.get();
    }

    @Override
    public List<Person> getNTeamMembersOrderedByTimeSpentInDescendingOrder(int N) {
        Pageable paging = of(0, N);
        return personRepository.getAllTPersonOrderedByTimeSpentInDescendingOrder(paging).getContent();
    }
}
