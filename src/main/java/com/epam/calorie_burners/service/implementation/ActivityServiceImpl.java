package com.epam.calorie_burners.service.implementation;


import com.epam.calorie_burners.model.Activity;
import com.epam.calorie_burners.repository.ActivityRepository;
import com.epam.calorie_burners.service.ActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    ActivityRepository activityRepository;

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public Activity registerActivity(Activity activity) {
        Activity storedActivity = activityRepository.save(activity);
        logger.info(storedActivity + " has been stored in the database.");
        return storedActivity;
    }

    @Override
    public long getTotalCaloriesBurnt() {
        return activityRepository.getTotalCaloriesBurnt();
    }

    @Override
    public double getTotalTimeSpentInHours() {
        int totalTimeSpentInMinutes = activityRepository.getTotalTimeSpentInMinutes();
        return totalTimeSpentInMinutes / 60.0;
    }

    @Override
    public boolean isThereRegisteredActivity() {
        return  activityRepository.isThereRegisteredActivity() != null;
    }
}
