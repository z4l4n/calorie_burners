package com.epam.calorie_burners.service.implementation;


import com.epam.calorie_burners.DTO.ActivityRegistrationDTO;
import com.epam.calorie_burners.DTO.DTOMapper;
import com.epam.calorie_burners.model.Activity;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.repository.ActivityRepository;
import com.epam.calorie_burners.service.ActivityService;
import com.epam.calorie_burners.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

@Service
public class ActivityServiceImpl implements ActivityService {

    private ActivityRepository activityRepository;

    private MemberService memberService;

    private DTOMapper dtoMapper;

    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository, MemberService memberService, DTOMapper dtoMapper) {
        this.activityRepository = activityRepository;
        this.memberService = memberService;
        this.dtoMapper = dtoMapper;
    }

    private static Logger logger = LoggerFactory.getLogger(ActivityService.class);


    @Override
    public void registerActivity(ActivityRegistrationDTO activityRegistrationDTO, OidcUser completersAccount) {
        Member completer = memberService.getMemberByEmail(completersAccount.getEmail());
        Activity activity = dtoMapper.mapActivityRegistrationDTOtoActivity(activityRegistrationDTO, completer);
        Activity storedActivity = activityRepository.save(activity);
        logger.info(storedActivity + " has been stored in the database.");

    }


    @Override
    public long getTotalCaloriesBurnt() {
        return activityRepository.getTotalCaloriesBurnt();
    }

    @Override
    public double getTotalTimeSpentInHours() {
        int totalTimeSpentInMinutes = activityRepository.getTotalTimeSpentInMinutes();
        return totalTimeSpentInMinutes / 60.0;
    }

    @Override
    public boolean isThereRegisteredActivity() {
        return activityRepository.isThereRegisteredActivity() != null;
    }

}
