package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.Activity;

public interface ActivityService {


    Activity registerActivity(Activity activity);

    long getTotalCaloriesBurnt();

    double getTotalTimeSpentInHours();

    boolean isThereRegisteredActivity();
}
