package com.epam.calorie_burners.service;

import com.epam.calorie_burners.DTO.ActivityRegistrationDTO;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

@Service
public interface ActivityService {

    void registerActivity(ActivityRegistrationDTO activity, OidcUser completedBy);

    long getTotalCaloriesBurnt();

    double getTotalTimeSpentInHours();

    boolean isThereRegisteredActivity();

}
