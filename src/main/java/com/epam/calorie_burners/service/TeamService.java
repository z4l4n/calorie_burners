package com.epam.calorie_burners.service;

import com.epam.calorie_burners.DTO.TeamPerformancePageDTO;
import com.epam.calorie_burners.controller.exception.AlreadyMemberOfATeamException;
import com.epam.calorie_burners.controller.exception.EmptyTeamNameException;
import com.epam.calorie_burners.controller.exception.NotFoundException;
import com.epam.calorie_burners.controller.exception.NotMemberOfATeamException;
import com.epam.calorie_burners.model.Member;
import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

public interface TeamService {

    long tryToStoreTeam(String teamName, OidcUser teamLeadersOidc) throws TeamAlreadyRegisteredException, AlreadyMemberOfATeamException, EmptyTeamNameException;

    String composeJoiningLinkURLSchema(HttpServletRequest request) throws URISyntaxException, MalformedURLException;

    Team findTeamById(String id) throws NotFoundException;

    long getTotalCaloriesBurnt(long teamId);

    double getTotalHoursSpent(long teamId);

    void updateTeam(Team team);

    void addMemberToTeam(Member member, Team team);

    List<Team> getTop5TeamsByTimeSpent();

    List<Long> getCaloriesBurntByTeams(List<Team> teams);

    List<Integer> getMinutesSpentWithActivityByTeams(List<Team> teams);

    long tryToGetTeamIdByEmail(String email) throws NotMemberOfATeamException;

    TeamPerformancePageDTO getTeamPerformancePageData(String teamId);

}
