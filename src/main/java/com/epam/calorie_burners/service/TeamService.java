package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.Team;
import com.epam.calorie_burners.service.exception.TeamAlreadyRegisteredException;

import javax.servlet.http.HttpServletRequest;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

public interface TeamService {

    Team registerTeam(Team team) throws TeamAlreadyRegisteredException;

    String composePersonalPageURLSchema(HttpServletRequest request) throws URISyntaxException, MalformedURLException;

    String composeTeamPageURLSchema(HttpServletRequest request) throws URISyntaxException, MalformedURLException;

    List<Team> getNTeamsOrderedByTimeSpentInDescendingOrder(int N);

    Team findTeamById(long id);

    long getTotalCaloriesBurnt(long teamId);

    double getTotalHoursSpent(long teamId);

}
