package com.epam.calorie_burners.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import javax.validation.constraints.NotNull;
import java.util.Set;

public interface UsersGrantedAuthoritiesService {
    @NotNull
    Set<GrantedAuthority> determineGrantedAuthoritiesFor(@NotNull final OidcUser user);

}
