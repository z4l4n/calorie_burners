package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.ActivityType;

import java.util.List;

public interface ActivityTypeService {
    List<String> getRegisteredActivityTypeNames();

    ActivityType getActivityTypeByName(String activityName);
}
