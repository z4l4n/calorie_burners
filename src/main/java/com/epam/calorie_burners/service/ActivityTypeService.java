package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.ActivityType;

public interface ActivityTypeService {
    Iterable<ActivityType> getActivityTypes();
}
