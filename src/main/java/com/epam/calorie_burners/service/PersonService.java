package com.epam.calorie_burners.service;

import com.epam.calorie_burners.model.Person;

import java.util.List;

public interface PersonService {

    Person findPersonById(long id);

    List<Person> getNTeamMembersOrderedByTimeSpentInDescendingOrder(int N);
}
