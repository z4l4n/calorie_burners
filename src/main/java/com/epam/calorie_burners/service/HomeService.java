package com.epam.calorie_burners.service;

import com.epam.calorie_burners.DTO.MemberScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TeamScoreboardItemDTO;
import com.epam.calorie_burners.DTO.TotalStatisticsDTO;

import java.util.List;

public interface HomeService {

    TotalStatisticsDTO getTotalStatistics();

    List<TeamScoreboardItemDTO> getTeamScoreBoard();

    List<MemberScoreboardItemDTO> getMemberScoreBoard();
}
