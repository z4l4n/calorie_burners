package com.epam.calorie_burners.service.exception;

public class TeamAlreadyRegisteredException extends Exception {
    public TeamAlreadyRegisteredException() {
        super();
    }

    public TeamAlreadyRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }
}
