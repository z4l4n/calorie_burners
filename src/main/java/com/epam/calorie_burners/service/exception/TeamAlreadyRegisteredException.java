package com.epam.calorie_burners.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TeamAlreadyRegisteredException extends Exception {
    public TeamAlreadyRegisteredException() {
        super();
    }

    public TeamAlreadyRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }

    public TeamAlreadyRegisteredException(String message) {
        super(message);
    }
}
